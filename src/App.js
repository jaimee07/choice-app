import React from 'react';
import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      first: null,
      second: null,
      third: null,
    };

    this.options = [
      // first group of radio-buttons
      [
        { id: '101', value: 'Vegetarian' },
        { id: '102', value: 'Nut allergy' },
        { id: '103', value: 'Halal' }
      ],
      // second group of radio-buttons
      [
        { id: '201', value: 'Cashew chicken' },
        { id: '202', value: 'Sweet and sour pork' },
        { id: '203', value: 'Stir fried Tofu' },
        { id: '204', value: 'Vegetable fried rice' },
        { id: '205', value: 'Pad Thai' },
        { id: '206', value: 'Massaman beef' },
      ],
      // third group of radio-buttons
      [
        { id: '301', value: 'Peanut sauce' },
        { id: '302', value: 'Oyster sauce' },
        { id: '303', value: 'Vegetable spring rolls' },
        { id: '304', value: 'Steamed rice' },
      ],
    ];

    this.rules = {
      // 'Vegetarian' is NOT compatible with 'Cashew chicken', 'Sweet and sour pork', 'Massaman beef', 'Oyster sauce'
      101: [201, 202, 206, 302], 
      // 'Nut allergy' is NOT compatible with 'Cashew chicken', 'Peanut sauce',
      102: [201, 301], 
      // 'Halal' is NOT compatible with 'Sweet and sour pork',
      103: [202], 
      // 'Vegetable fried rice' is NOT compatible with 'Steamed rice' (you don't need more rice... carb overload),
      204: [304],
      // 'Pad thai' is NOT compatible with 'Steamed rice' (Pad thai comes with noodles),
      205: [304],
    };
  }

  changeHandler = (event) => {
    this.setState({[event.target.name]: event.target.value});

    if (event.target.name === 'first') {
      this.setState({'second': '', 'third': ''});
    }

    if (event.target.name === 'second') {
      this.setState({ 'third': ''});
    }
  }

  submitHandler = (event) => {
    event.preventDefault();
    alert(
      'Congratulations. Your meal will be delivered to you shortly. ;)'
    );
  }

  renderFirstOptions() {
    const firstOptions = this.options[0].map((option) => 
      <span className="Options">
        <input
          type="radio"
          name="first"
          value={option.id}
          onChange={this.changeHandler}
        /> {option.value}
      </span>
    );
    return(
      <div className="OptionsSection">
        {firstOptions}
      </div>
    );
  }

  renderSecondOptions() {
    const secondOptions = this.options[1].map((option) => {
      const disabled = (this.rules[Number(this.state.first)] && this.rules[Number(this.state.first)].includes(Number(option.id)));

      return (
        <span className="Options">
          <input 
            key={option.id}
            type="radio"
            name="second"
            value={option.id}
            disabled={disabled}
            checked={this.state.second === option.id}
            onChange={this.changeHandler}
          /> {option.value}
        </span>
      );
    });

    return (
      <div className="OptionsSection">
        {secondOptions}
      </div>
    );
  }

  renderThirdOptions() {
    const thirdOptions = this.options[2].map((option) => {
      const disabled = (this.rules[Number(this.state.first)] && this.rules[Number(this.state.first)].includes(Number(option.id))) ||
        (this.rules[Number(this.state.second)] && this.rules[Number(this.state.second)].includes(Number(option.id)));

      return (
        <span className="Options">
          <input
            key={option.id}
            type="radio"
            name="third"
            value={option.id}
            disabled={disabled}
            checked={this.state.third === option.id}
            onChange={this.changeHandler}
          /> {option.value}
        </span>
      );
    });

    return(
      <div className="OptionsSection">
        {thirdOptions}
      </div>
    );
  }

  render() {
    return (
      <div className="container">
        <div className="row">
        <form className="App" onSubmit={this.submitHandler}>
          <h1 className="Greeting">
            Hello, please choose your meal preferences.
          </h1>
          
          {this.renderFirstOptions()}
          {this.state.first && this.renderSecondOptions()}
          {this.state.second && this.renderThirdOptions()}

          <button className="button-primary" type="submit" disabled={!this.state.third}>
            Submit
          </button>
        </form>
        </div>
      </div>
    );
  }
}

export default App;
